from django.conf.urls import patterns, url
from django.views.decorators.csrf import csrf_exempt
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from coderush import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ktj14.views.home', name='home'),
    url(r'^$', 'coderush.views.coderush'),
    url(r'^auth2/$','coderush.views.auth2',name='src_auth'),
    url(r'^check$','coderush.views.check',name='check'),
    url(r'^start1/auth/$','coderush.views.coderushstart1_auth',name='DIS'),
    url(r'^start2/auth/$','coderush.views.coderushstart2_auth',name='DIS'),
    url(r'^start3/auth/$','coderush.views.coderushstart3_auth',name='DIS'),
    url(r'^start4/auth/$','coderush.views.coderushstart4_auth',name='DIS'),
    url(r'^start5/auth/$','coderush.views.coderushstart5_auth',name='DIS'),
    url(r'^start6/auth/$','coderush.views.coderushstart6_auth',name='DIS'),
    # url(r'^coderushstart$', 'coderush.views.coderushstart'),
    url(r'^codesubmit$','coderush.views.codesubmit',name='codesubmit'),

)
