from django.contrib import admin
from coderush.models import *
# Register your models here.

class CoderushAdmin(admin.ModelAdmin):
    list_display = ('user','points')

admin.site.register(Coderush,CoderushAdmin)
admin.site.register(CoderushUser)
admin.site.register(CoderushAnswers)