from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from django.http.response import HttpResponse, HttpResponseRedirect, Http404
from django.views.decorators.csrf import csrf_exempt
from event.models import *
from journal.models import *
from myktj.models import *
from sponsor.models import *
from myktj.forms import *
from coderush.models import *
from datetime import timedelta,datetime
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.core import serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser,MultiPartParser,FormParser
from django.contrib.auth.models import User
import json
from django.conf import settings
from django.core.files.storage import FileSystemStorage,default_storage
import os, sys
# import requests

i = 0
def coderush(request ):
  # user=request.user
  # if user.groups.filter(name='coderush').exists():
  #   if len(CoderushUser.objects.filter(user = user))==0:
  #       play = CoderushUser (user=user)
  #       play.save()
  #   if len(Coderush.objects.filter(user = user))==0:
  #       play = Coderush (user=user)
  #       play.save()

  #   #if len(timestart)>0:
  #    #   return HttpResponseRedirect('/coderush/start1')
  #   return render(request,'coderush.html')
  # else:
  #   return HttpResponse("Not authorised to view the page")
    return render(request,'coderush.html')

@api_view(['POST'])
def auth2(request):
  username = request.data.get('username')
  user=User.objects.get(username=username)
  print user.groups.filter(name='coderush').exists()
  if user.groups.filter(name='coderush').exists():

    if len(CoderushUser.objects.filter(user = user))==0:
        play = CoderushUser (user=user)
        play.save()
    if len(Coderush.objects.filter(user = user))==0:
        play = Coderush (user=user)
        play.save()

    #if len(timestart)>0:
     #   return HttpResponseRedirect('/coderush/start1')
    return Response({"success":1})
  else:
    return Response({"success":0})

def coderushstart1 (request):
    return render(request,'coderushstart1.html')


@api_view(['POST'])
@never_cache
def coderushstart1_auth (request):
  username = request.data.get('username')
  user=User.objects.get(username=username)
  if user.groups.filter(name='coderush').exists():
    answersAll = Coderush.objects.filter(user = user)


    bid= Coderush.objects.filter(user = user)[0]
    gamestatus = CoderushUser.objects.filter(user = user)[0]
    if bid.clickquestion1==0:

        firstclick = datetime.now()
        minremain = 00
        secremain = 00
        Coderush.objects.filter(user = user).update(clickquestion1=1)
        CoderushUser.objects.filter(user = user).update(last_played=firstclick)
    #if len(answersAll)>0:
        #return HttpResponseRedirect('/coderush/start1')
    if gamestatus.gameover == 1:
        return Response({'gameover':1})
    if len(CoderushUser.objects.filter(user = user))>0:
        playstart = CoderushUser.objects.filter(user = user)[0]
        if playstart.last_played > timezone.now()-timedelta(minutes = 30):
            timeremain = timezone.now()-playstart.last_played
            minremain = int((timeremain.seconds)/60)
            secremain = int((timeremain.seconds)%60)
            if(bid.bid1==1):
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
            else :
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})
        else:
            playstart.gameover = 1
            playstart.save()
            return Response({'gameover':1})
    gamestatus.gameover = 1
    gamestatus.save()

    if minremain == 00 and secremain == 00 :
        i = i+1
        print i

    if(bid.bid1==1):
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
    else :
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})

def coderushstart2 (request):
  return render(request,'coderushstart2.html')

@api_view(['POST'])
@never_cache
def coderushstart2_auth (request):
  username = request.data.get('username')
  user=User.objects.get(username=username)
  if user.groups.filter(name='coderush').exists():
    answersAll = Coderush.objects.filter(user = user)


    bid= Coderush.objects.filter(user = user)[0]
    gamestatus = CoderushUser.objects.filter(user = user)[0]
    if bid.clickquestion2==0:

        firstclick = datetime.now()
        minremain = 00
        secremain = 00
        Coderush.objects.filter(user = user).update(clickquestion2=1)
        CoderushUser.objects.filter(user = user).update(last_played2=firstclick)
    #if len(answersAll)>0:
        #return HttpResponseRedirect('/coderush/start1')
    if gamestatus.gameover2 == 1:
        return Response({'gameover2':1})
    if len(CoderushUser.objects.filter(user = user))>0:
        playstart = CoderushUser.objects.filter(user = user)[0]
        if playstart.last_played2 > timezone.now()-timedelta(minutes = 30):
            timeremain = timezone.now()-playstart.last_played2
            minremain = int((timeremain.seconds)/60)
            secremain = int((timeremain.seconds)%60)
            if(bid.bid2==1):
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
            else :
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})
        else:
            playstart.gameover2 = 1
            playstart.save()
            return Response({'gameover2':1})
    gamestatus.gameover2 = 1
    gamestatus.save()
    if minremain == 00 and secremain == 00:
        i = i+1
    if(bid.bid2==1):
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
    else :
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})


def coderushstart3 (request):
    return render(request,'coderushstart3.html')


@api_view(['POST'])
@never_cache
def coderushstart3_auth (request):
  username = request.data.get('username')
  user=User.objects.get(username=username)
  if user.groups.filter(name='coderush').exists():
    answersAll = Coderush.objects.filter(user = user)


    bid= Coderush.objects.filter(user = user)[0]
    gamestatus = CoderushUser.objects.filter(user = user)[0]
    if bid.clickquestion3==0:

        firstclick = datetime.now()
        minremain = 00
        secremain = 00
        Coderush.objects.filter(user = user).update(clickquestion3=1)
        CoderushUser.objects.filter(user = user).update(last_played3=firstclick)
    #if len(answersAll)>0:
        #return HttpResponseRedirect('/coderush/start1')
    if gamestatus.gameover3 == 1:
        return Response({'gameover3':1})
    if len(CoderushUser.objects.filter(user = user))>0:
        playstart = CoderushUser.objects.filter(user = user)[0]
        if playstart.last_played3 > timezone.now()-timedelta(minutes = 30):
            timeremain = timezone.now()-playstart.last_played3
            minremain = int((timeremain.seconds)/60)
            secremain = int((timeremain.seconds)%60)
            if(bid.bid3==1):
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
            else :
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})
        else:
            playstart.gameover3 = 1
            playstart.save()
            return Response({'gameover3':1})
    gamestatus.gameover3 = 1
    gamestatus.save()
    if minremain == 00 and secremain == 00:
        i = i+1
    if(bid.bid3==1):
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
    else :
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})


def coderushstart4 (request):
    return render(request,'coderushstart4.html')


@api_view(['POST'])
@never_cache
def coderushstart4_auth (request):
  username = request.data.get('username')
  user=User.objects.get(username=username)
  if user.groups.filter(name='coderush').exists():
    answersAll = Coderush.objects.filter(user = user)


    bid= Coderush.objects.filter(user = user)[0]
    gamestatus = CoderushUser.objects.filter(user = user)[0]
    if bid.clickquestion4==0:

        firstclick = datetime.now()
        minremain = 00
        secremain = 00
        Coderush.objects.filter(user = user).update(clickquestion4=1)
        CoderushUser.objects.filter(user = user).update(last_played4=firstclick)
    #if len(answersAll)>0:
        #return HttpResponseRedirect('/coderush/start1')
    if gamestatus.gameover4 == 1:
        return Response({'gameover4':1})
    if len(CoderushUser.objects.filter(user = user))>0:
        playstart = CoderushUser.objects.filter(user = user)[0]
        if playstart.last_played4 > timezone.now()-timedelta(minutes = 30):
            timeremain = timezone.now()-playstart.last_played4
            minremain = int((timeremain.seconds)/60)
            secremain = int((timeremain.seconds)%60)
            if(bid.bid4==1):
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
            else :
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})
        else:
            playstart.gameover4 = 1
            playstart.save()
            return Response({'gameover4':1})
    gamestatus.gameover4 = 1
    gamestatus.save()
    if minremain == 00 and secremain == 00:
        i= i+1
    if(bid.bid4==1):
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
    else :
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})



def coderushstart5 (request):
    if i == 1:
        return render(request,'coderushstart5.html')
    else:
        return render(request,'coderush.html')


@api_view(['POST'])
@never_cache
def coderushstart5_auth (request):
  username = request.data.get('username')
  user=User.objects.get(username=username)
  if user.groups.filter(name='coderush').exists():
    answersAll = Coderush.objects.filter(user = user)


    bid= Coderush.objects.filter(user = user)[0]
    gamestatus = CoderushUser.objects.filter(user = user)[0]
    if bid.clickquestion5==0:

        firstclick = datetime.now()
        minremain = 00
        secremain = 00
        Coderush.objects.filter(user = user).update(clickquestion5=1)
        CoderushUser.objects.filter(user = user).update(last_played5=firstclick)
    #if len(answersAll)>0:
        #return HttpResponseRedirect('/coderush/start1')
    if gamestatus.gameover5 == 1:
        return Response({'gameover5':1})
    if len(CoderushUser.objects.filter(user = user))>0:
        playstart = CoderushUser.objects.filter(user = user)[0]
        if playstart.last_played5 > timezone.now()-timedelta(minutes = 30):
            timeremain = timezone.now()-playstart.last_played5
            minremain = int((timeremain.seconds)/60)
            secremain = int((timeremain.seconds)%60)
            if(bid.bid5==1):
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
            else :
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})
        else:
            playstart.gameover5 = 1
            playstart.save()
            return Response({'gameover5':1})
    gamestatus.gameover5 = 1
    gamestatus.save()
    if(bid.bid5==1):
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
    else :
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})


def coderushstart6 (request):
    return render(request,'coderushstart6.html')


@api_view(['POST'])
@never_cache
def coderushstart6_auth (request):
  username = request.data.get('username')
  user=User.objects.get(username=username)
  if user.groups.filter(name='coderush').exists():
    answersAll = Coderush.objects.filter(user = user)


    bid= Coderush.objects.filter(user = user)[0]
    gamestatus = CoderushUser.objects.filter(user = user)[0]
    if bid.clickquestion6==0:

        firstclick = datetime.now()
        minremain = 00
        secremain = 00
        Coderush.objects.filter(user = user).update(clickquestion6=1)
        CoderushUser.objects.filter(user = user).update(last_played6=firstclick)
    #if len(answersAll)>0:
        #return HttpResponseRedirect('/coderush/start1')
    if gamestatus.gameover6 == 1:
        return Response({'gameover6':1})
    if len(CoderushUser.objects.filter(user = user))>0:
        playstart = CoderushUser.objects.filter(user = user)[0]
        if playstart.last_played6 > timezone.now()-timedelta(minutes = 30):
            timeremain = timezone.now()-playstart.last_played6
            minremain = int((timeremain.seconds)/60)
            secremain = int((timeremain.seconds)%60)
            if(bid.bid6==1):
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
            else :
              return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})
        else:
            playstart.gameover6 = 1
            playstart.save()
            return Response({'gameover6':1})
    gamestatus.gameover6 = 1
    gamestatus.save()
    if(bid.bid6==1):
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':1})
    else :
      return Response({'minremain':minremain,'points':bid.points, 'secremain':secremain,'start':0})

@never_cache
def coderushsuccess(request ):
  return render(request,'coderushsuccess.html')
@never_cache
def coderushdone(request ):
  return render(request,'coderushdone.html')
@never_cache
def coderushsubmit(request):
  username = request.POST.get('bidusername')
  user=User.objects.get(username=username)
  if user.groups.filter(name='coderush').exists():
    gamestatus = CoderushUser.objects.filter(user = user)[0]
    answersAll = Coderush.objects.filter(user = user)
    #if gamestatus.gameover == 1:
     #   return HttpResponseRedirect('/Coderush/done')
    playstart = CoderushUser.objects.filter(user = user)[0]
    bidded = Coderush.objects.filter(user = user)[0]


    if len(answersAll)==0:
        if request.method =="POST":
            v=request.POST.get('number')
            points = request.POST.get('points')
            bidded_points = int(bidded.points)
            # print points, type(points)
            newpoints =  bidded_points - int(points)
            if 0:
            	return redirect('https://www.codechef.com/')
            else:
	            if v=='1':
	            	if playstart.last_played > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid1:
                		return redirect('https://www.codechef.com/')
	            	Coderushanswer = Coderush(user=user, question1=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid1=1)
	            	Coderushanswer.save()
	            	return Response({'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='2':
	            	if playstart.last_played2 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played2
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid2:
                		return redirect('https://www.codechef.com/')
	            	Coderushanswer = Coderush(user=user, question2=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid2=1)
	            	Coderushanswer.save()
	            	return render(request, 'coderushstart2.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='3':
	            	if playstart.last_played3 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played3
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid3:
                		return redirect('https://www.google.com/')
	            	Coderushanswer = Coderush(user=user, question3=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid3=1)
	            	Coderushanswer.save()
	            	return render(request, 'coderushstart3.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='4':
	            	if playstart.last_played4 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played4
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid4:
                		return redirect('https://www.google.com/')
	            	Coderushanswer = Coderush(user=user, question4=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid4=1)
	            	Coderushanswer.save()
	            	return render(request, 'coderushstart4.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='5':
	            	if playstart.last_played5 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played5
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid5:
                		return redirect('https://www.google.com/')
	            	Coderushanswer = Coderush(user=user, question5=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid5=1)
	            	Coderushanswer.save()
	            	return render(request, 'coderushstart5.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='6':
	            	if playstart.last_played6 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played6
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid6:
                		return redirect('https://www.google.com/')
	            	Coderushanswer = Coderush(user=user, question6=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid6=1)
	            	Coderushanswer.save()
	            	return render(request, 'coderushstart6.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
    else:
        if request.method =="POST":
            v=request.POST.get('number')
            points = request.POST.get('points')
            bidded_points = int(bidded.points)
            newpoints =  bidded_points - int(points)
            if 0:
            	return redirect('https://www.codechef.com/')
            else:
	            if v=='1':
	            	if playstart.last_played > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid1:
                		return redirect('https://www.codechef.com/')
	            	Coderushanswer = Coderush(user=user, question1=points)
	            	Coderush.objects.filter(user = user).update(question1=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid1=1)
	            	return render(request, 'coderushstart1.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='2':
	            	if playstart.last_played2 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played2
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid2:
                		return redirect('https://www.codechef.com/')
	            	Coderushanswer = Coderush(user=user, question2=points)
	            	Coderush.objects.filter(user = user).update(question2=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid2=1)
	            	return render(request, 'coderushstart2.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='3':
	            	if playstart.last_played3 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played3
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid3:
                		return redirect('https://www.codechef.com/')
	            	Coderushanswer = Coderush(user=user, question3=points)
	            	Coderush.objects.filter(user = user).update(question3=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid3=1)
	            	return render(request, 'coderushstart3.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='4':
	            	if playstart.last_played4 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played4
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid4:
                		return redirect('https://www.google.com/')
	            	Coderushanswer = Coderush(user=user, question4=points)
	            	Coderush.objects.filter(user = user).update(question4=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid4=1)
	            	return render(request, 'coderushstart4.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='5':
	            	if playstart.last_played5 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played5
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid5:
                		return redirect('https://www.google.com/')
	            	Coderush.objects.filter(user = user).update(question5=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid5=1)
	            	return render(request, 'coderushstart5.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
	            elif v=='6':
	            	if playstart.last_played6 > timezone.now()-timedelta(minutes = 30):
        		        timeremain = timezone.now()-playstart.last_played6
        		        minremain = int((timeremain.seconds)/60)
        		        secremain = int((timeremain.seconds)%60)
    		        else :
    		            minremain=0
    		            secremain=0
	            	if bidded.bid6:
                		return redirect('https://www.google.com/')
	            	Coderush.objects.filter(user = user).update(question6=points)
	            	Coderush.objects.filter(user = user).update(points=newpoints, bid6=1)
	            	return render(request, 'coderushstart6.html', {'minremain':minremain,'points':bidded.points, 'secremain':secremain,'start':1})
  else:
    return HttpResponse("Not authorised to view the page")

@api_view(['GET'])
def check(request):
    import requests
    # constants
    COMPILE_URL = u'http://api.hackerearth.com/code/compile/'
    RUN_URL = u'http://api.hackerearth.com/code/run/'
    CLIENT_SECRET = '1e02f32ecd18f81e02445002868992d9eadaaeaf'

    source = open('/home/soumyadeep/Desktop/test.c','r')

    data = {
        'client_secret': CLIENT_SECRET,
        'async': 0,
        # 'source': 'name = input("Whats your name? ")\nprint("fffyf"+name)\nprint("hellffdvfvf")\nprint("hellodererefrfefrfeferrf")',
        'source': source.read(),
        'lang': "C",
        'time_limit': 5,
        'memory_limit': 262144,
        'input':'2\n3',
    }
    # r1 = requests.post(COMPILE_URL, data=data)
    r = requests.post(RUN_URL, data=data)
    source.close()
    print r.json()
    x = r.json()
    return Response(x)

def save_file(file,userid,q):
  fs = FileSystemStorage()
  filename = fs.save('coderush_'+str(userid)+"_q"+str(q), file)
  fs.url(filename)
  return filename,fs

def codesubmit(request):
    if request.method == 'POST' and request.FILES['codefile']:
        q=request.POST.get('quenumber')
        username=request.POST.get('queusername')
        user1 = User.objects.get(username=username)
        user= Coderush.objects.get(user=user1)
        user2= CoderushUser.objects.get(user=user1)
        # restuser= CoderushUser.objects.all()
        lang = request.POST.get('language')
        myfile = request.FILES['codefile']
        questions=CoderushAnswers.objects.get(id=1)
        filename,fs = save_file(myfile,user.id,q)
        filename1 = filename[:-8]
        uploaded_file_url = fs.url(filename1)
        saved_file_url='/home/soumyadeep/latest/ktj17_dev'+uploaded_file_url
        current_file_url='/home/soumyadeep/latest/ktj17_dev/mediafile/'+filename
        print uploaded_file_url
        import requests
        if (default_storage.exists(saved_file_url)):
          if (str(filename)!=saved_file_url):
            default_storage.delete(current_file_url)
            print 2
          print 2
        # constants
        COMPILE_URL = u'http://api.hackerearth.com/code/compile/'
        RUN_URL = u'http://api.hackerearth.com/code/run/'
        CLIENT_SECRET = '82cd6659aa9071cf15074b4fdb7b10693752bf36'
        #print questions.input1.split("\\n")[0]+'\n'+questions.input1.split("\\n")[1]
        # head=uploaded_file_url.split('_',1)[0]
        # print head
        y=0
        for x in xrange(1,4):
          if x==1:
            source = open(saved_file_url,'r')
            input1= questions.input1.split()[0]+'\n'+questions.input1.split()[1]
            data = {
                  'client_secret': CLIENT_SECRET,
                  'async': 0,
                  # 'source': 'name = input("Whats your name? ")\nprint("fffyf"+name)\nprint("hellffdvfvf")\nprint("hellodererefrfefrfeferrf")',
                  'source': source.read(),
                  'lang': lang,
                  'time_limit': 5,
                  'memory_limit': 262144,
                  'input':input1,
            }
            r = requests.post(RUN_URL, data=data)
            source.close()
            if q=='1':
              user2.file1=myfile
              user2.save()
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output1):
                      y+=1
              else:
                  return HttpResponse('False1')

            if q=='2':
              user2.file2=myfile
              user2.save()
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output1):
                      y+=1
              else:
                  return HttpResponse('False1')

            if q=='3':
              user2.file3=myfile
              user2.save()
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output1):
                      y+=1
              else:
                  return HttpResponse('False1')

            if q=='4':
              user2.file4=myfile
              user2.save()
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output1):
                      y+=1
              else:
                  return HttpResponse('False1')

            if q=='5':
              user2.file5=myfile
              user2.save()
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output1):
                      y+=1
              else:
                  return HttpResponse('False1')

            if q=='6':
              user2.file6=myfile
              user2.save()
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output1):
                      y+=1
              else:
                  return HttpResponse('False1')


          if x==2:
            source = open(saved_file_url,'r')
            input2=questions.input2.split()[0]+'\n'+questions.input2.split()[1]
            data = {
                  'client_secret': CLIENT_SECRET,
                  'async': 0,
                  # 'source': 'name = input("Whats your name? ")\nprint("fffyf"+name)\nprint("hellffdvfvf")\nprint("hellodererefrfefrfeferrf")',
                  'source': source.read(),
                  'lang': lang,
                  'time_limit': 5,
                  'memory_limit': 262144,
                  'input':input2,
            }
            r = requests.post(RUN_URL, data=data)
            source.close()
            if q=='1':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output2):
                    y+=1
              else:
                  return HttpResponse('False1')

            if q=='2':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output2):
                    y+=1
              else:
                  return HttpResponse('False1')

            if q=='3':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output2):
                    y+=1
              else:
                  return HttpResponse('False1')

            if q=='4':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output2):
                    y+=1
              else:
                  return HttpResponse('False1')

            if q=='5':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output2):
                    y+=1

              else:
                  return HttpResponse('False1')

            if q=='6':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output2):
                      y+=1
              else:
                  return HttpResponse('False1')
          if x==3:
            source = open(saved_file_url,'r')
            input3=questions.input3.split()[0]+'\n'+questions.input3.split()[1]
            data = {
                  'client_secret': CLIENT_SECRET,
                  'async': 0,
                  # 'source': 'name = input("Whats your name? ")\nprint("fffyf"+name)\nprint("hellffdvfvf")\nprint("hellodererefrfefrfeferrf")',
                  'source': source.read(),
                  'lang': lang,
                  'time_limit': 5,
                  'memory_limit': 262144,
                  'input':input3,
            }
            r = requests.post(RUN_URL, data=data)
            source.close()
            if q=='1':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output3):
                    if y==2:
                      user.points=user.points+3*(user.question1)

                      for p in xrange(1,3):
                        i = Coderush.objects.get(pk=p)
                        i.points = i.points - user.question1
                        i.save()


                      user2.answer1=2
                      user.save()
                      user2.save()
                      return HttpResponse('True')
                  else:
                      user2.answer1==1
                      user2.save()
                      return HttpResponse('False2')
              else:
                  return HttpResponse('False1')

            if q=='2':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output3):
                    if y==2:
                      user.points=user.points+2*(user.question2)
                      for p in xrange(1,20):
                        i = Coderush.objects.get(pk=p)
                        i.points = i.points - user.question1
                        i.save()

                      user2.answer2=2
                      user2.save()
                      user.save()
                      return HttpResponse('True')
                  else:
                      user2.answer2=1
                      user2.save()
                      return HttpResponse('False2')
              else:
                  return HttpResponse('False1')

            if q=='3':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output3):
                    if y==2:
                      user.points=user.points+3*(user.question1)
                      for p in xrange(1,2):
                        i = Coderush.objects.get(pk=p)
                        i.points = i.points - user.question1
                        i.save()

                      user2.answer1=2
                      user.save()
                      user2.save()
                      return HttpResponse('True')
                  else:
                      user2.answer3=1
                      user2.save()
                      return HttpResponse('False2')

              else:
                  return HttpResponse('False1')

            if q=='4':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output3):
                    if y==2:
                      user.points=user.points+3*(user.question4)
                      for p in xrange(1,2):
                        i = Coderush.objects.get(pk=p)
                        i.points = i.points - user.question1
                        i.save()

                      user2.answer4=2
                      user2.save()
                      user.save()
                      return HttpResponse('True')
                  else:
                      user2.answer4=1
                      user2.save()
                      return HttpResponse('False2')

              else:
                  return HttpResponse('False1')

            if q=='5':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output3):
                    if y==2:
                      user.points=user.points+2*(user.question5)
                      for p in xrange(1,2):
                        i = Coderush.objects.get(pk=p)
                        i.points = i.points - user.question1
                        i.save()

                      user2.answer5=2
                      user2.save()
                      user.save()
                      return HttpResponse('True')
                  else:
                      user2.answer5=1
                      user2.save()
                      return HttpResponse('False2')

              else:
                  return HttpResponse('False1')

            if q=='6':
              if r.json()['run_status']['status']=='AC':
                  if int(r.json()['run_status']['output'])==int(questions.output3):
                    if y==2:
                      user.points=user.points+2*(user.question6)
                      for p in xrange(1,2):
                        i = Coderush.objects.get(pk=p)
                        i.points = i.points - user.question1
                        i.save()

                      user2.answer6=2
                      user2.save()
                      user.save()
                      return HttpResponse('True')
                  else:
                      user2.answer6=1
                      user2.save()
                      return HttpResponse('False2')

              else:
                  return HttpResponse('False1')
    #     x = json.dumps(r.json())
    #     # return render(request, 'coderush.html', {'output':x})

    #     return HttpResponse(x['run_status'], content_type='application/json')
    # return render(request, 'coderushstart1.html')
