# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coderush', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='coderush',
            name='user',
        ),
        migrations.DeleteModel(
            name='coderush',
        ),
        migrations.RemoveField(
            model_name='coderushuser',
            name='user',
        ),
        migrations.DeleteModel(
            name='coderushUser',
        ),
    ]
