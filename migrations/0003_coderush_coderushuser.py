# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('coderush', '0002_auto_20160116_2301'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coderush',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('points', models.IntegerField(default=1000)),
                ('question1', models.IntegerField(default=0)),
                ('question2', models.IntegerField(default=0)),
                ('question3', models.IntegerField(default=0)),
                ('question4', models.IntegerField(default=0)),
                ('question5', models.IntegerField(default=0)),
                ('question6', models.IntegerField(default=0)),
                ('clickquestion1', models.IntegerField(default=0)),
                ('clickquestion2', models.IntegerField(default=0)),
                ('clickquestion3', models.IntegerField(default=0)),
                ('clickquestion4', models.IntegerField(default=0)),
                ('clickquestion5', models.IntegerField(default=0)),
                ('clickquestion6', models.IntegerField(default=0)),
                ('bid1', models.IntegerField(default=0)),
                ('bid2', models.IntegerField(default=0)),
                ('bid3', models.IntegerField(default=0)),
                ('bid4', models.IntegerField(default=0)),
                ('bid5', models.IntegerField(default=0)),
                ('bid6', models.IntegerField(default=0)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='CoderushUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_played', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('last_played2', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('last_played3', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('last_played4', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('last_played5', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('last_played6', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('gameover', models.IntegerField(default=0, blank=True)),
                ('gameover2', models.IntegerField(default=0, blank=True)),
                ('gameover3', models.IntegerField(default=0, blank=True)),
                ('gameover4', models.IntegerField(default=0, blank=True)),
                ('gameover5', models.IntegerField(default=0, blank=True)),
                ('gameover6', models.IntegerField(default=0, blank=True)),
                ('answer1', models.IntegerField(default=0, blank=True)),
                ('answer2', models.IntegerField(default=0, blank=True)),
                ('answer3', models.IntegerField(default=0, blank=True)),
                ('answer4', models.IntegerField(default=0, blank=True)),
                ('answer5', models.IntegerField(default=0, blank=True)),
                ('answer6', models.IntegerField(default=0, blank=True)),
                ('file1', models.FileField(upload_to=b'')),
                ('file2', models.FileField(upload_to=b'')),
                ('file3', models.FileField(upload_to=b'')),
                ('file4', models.FileField(upload_to=b'')),
                ('file5', models.FileField(upload_to=b'')),
                ('file6', models.FileField(upload_to=b'')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
        ),
    ]
